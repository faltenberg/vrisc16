VRISC16
=======

VRISC16 shall become a 16bit RISC-processor. It's purpose is educational and shall grant insight
into computer architecture and microelectronics.

For now VRISC16 is only simulated but in future it shall be implemented in real hardware.


Running the Simulation
======================

The processor is simulated in the [Logisim](http://www.cburch.com/logisim/) simulation tool (runs
on Java). Please download and install the software and open the *vrisc16.circ* file. Please visit
the Logisim website or search for Youtube tutorials to learn how to use the simulator. Read the
instructions for the VRISC16 user interface.


Motivation
==========

While there are lots of books and tutorials on the inner working of CPUs (registers, ALU,
pipelining), there is little information how the cache is actually implemented or how the
connection to devices is established. Therefore I decided to design and build an own processor.
I went for a 16bit architecture because 8bit are too restrictive in the number of instrucions
(especially in the case of a RISC architecture) and would need a 16bit adressing anyway (256 bytes
are simply not practical). I decided not to use 32bits since most modern processors are 32bit and
also this doesn't restrict the instructions (32bit processors have lots of fancy optimizations fit
into one instruction, like barrel shifting of immediate values). Thus, 16bit is a good tradeoff
between large memory and simplicity while it still grants opportunity to puzzle over how to fit
3-register-addressing or immediate values into the instruction format.

VRISC16 is oriented on the [HERA](https://www.haverford.edu/computer-science/resources/hera)
processor architecture which is also a 16bit RISC-processor with 4bit instruction opcode and 16
registers. The instructions use 3-register-addressing and there are no immediate versions of ALU
operations.
